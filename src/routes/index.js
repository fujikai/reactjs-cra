import React from 'react'
import { Route } from 'react-router'
import AppPage from '../containers/AppPage'

export default <Route path="/" component={ AppPage }>
</Route>
