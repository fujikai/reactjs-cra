---
# ReactJS - Advance Template
###### A template that uses Create-React-App tool, with no configurations needed 
[Create-React-App](https://github.com/facebookincubator/create-react-app)

## Running App
### Development Mode:
`npm start`
### Interactive Test Watch:
`npm test`
### Production Build:
`npm run build`

## Development Dependencies
### Babel Polyfill:
`babel-polyfill`
### Redux Dev Tools
`redux-devtools`
### React Scripts
`react-scripts`

## Dependencies
### React Router:
`react-router`
### Redux:
`redux`
### React Redux:
`react-redux`
### Redux Thunk
`redux-thunk`
### React Thunk
`react-thunk`
### Redux Logger
`redux-logger`
### React Bootstrap
`react-bootstrap`
### Bootstrap 3
`bootstrap@3`
---